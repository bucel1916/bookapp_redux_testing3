import { dataReducers } from '../../../src/redux/reducer';
import {
  GET_BOOKS_ID, GET_BOOKS_POPULAR, GET_BOOKS_RECOMMENDED, SET_LOADING, SET_REFRESHING,
} from '../../../src/redux/types';

describe('dataReducers', () => {
  it('should return the initial state', () => {
    expect(dataReducers(undefined, {})).toEqual({
      booksRecommended: [],
      booksPopular: [],
      isLoading: false,
      isOnline: true,
      booksId: [],
      isRefreshing: false,
    });
  });
  describe('GET_BOOKS_ID', () => {
    it('should handle GET_BOOKS_ID', () => {
      expect(dataReducers({}, {
        type: GET_BOOKS_ID,
        payload: [],
      })).toEqual({
        booksId: [],
        isLoading: false,
        isRefreshing: false,
      });
    });
  });

  describe('GET_BOOKS_POPULAR', () => {
    it('should handle GET_BOOKS_POPULAR', () => {
      expect(dataReducers({}, {
        type: GET_BOOKS_POPULAR,
        payload: [],
      })).toEqual({
        booksPopular: [],
        isLoading: false,
        isRefreshing: false,
      });
    });
  });

  describe('GET_BOOKS_RECOMMENDED', () => {
    it('should handle GET_BOOKS_RECOMMENDED', () => {
      expect(dataReducers({}, {
        type: GET_BOOKS_RECOMMENDED,
        payload: [],
      })).toEqual({
        booksRecommended: [],
        isLoading: false,
        isRefreshing: false,
      });
    });
  });
  describe('SET_LOADING', () => {
    it('should handle SET_LOADING', () => {
      expect(dataReducers({}, {
        type: SET_LOADING,
        payload: true,
      })).toEqual({
        isLoading: true,
      });
    });
  });
  describe('SET_REFRESHING', () => {
    it('should handle SET_REFRESHING', () => {
      expect(dataReducers({}, {
        type: SET_REFRESHING,
        payload: true,
      })).toEqual({
        isRefreshing: true,
      });
    });
  });
});
