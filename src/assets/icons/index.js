import ICLike from './like.svg';
import ICShare from './share.svg';
import ICBack from './back.svg';
import ICCheck from './check.svg';
import ICLogout from './logout.svg';

export {
  ICLike, ICBack, ICShare, ICCheck, ICLogout,
};
